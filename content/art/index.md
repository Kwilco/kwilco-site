---
title: "Art showcase"
summary: "A showcase of some of my original artwork."
date: 2020-07-02T18:36:22-07:00
draft: false
---

This is some of my original art. It's mostly anime and video game fan art. I've
linked to relevant character bios, where you'll also find a lot of the reference
material I've used. I'm using a Wacom Cintiq 16 drawing tablet and
[Clip Studio Paint][csp].

These are in reverse chronological order (aka newest first).

You can **Right click > View Image** (or your browser's equivalent) to view
images at a larger size.

---

The goddess [Eris](https://konosuba.fandom.com/wiki/Eris) from Konosuba.

![Eris smiling][eris]

---

A young [Katarina Claes](https://bakarina.fandom.com/wiki/Katarina_Claes) after
doing some gardening.

![Katarina Claes gardening][katarina]

---

Cut-in of [Futaba Sakura](https://megamitensei.fandom.com/wiki/Futaba_Sakura).

![Futaba face close-up][futaba]

---

The [handler](https://monsterhunter.fandom.com/wiki/Handler) after a successful
hunt.

![Handler is happy][handler]

---

[Megumin](https://konosuba.fandom.com/wiki/Megumin) poking a frog.

![Megumin poking a frog][megumin2]

---

[Aqua](https://konosuba.fandom.com/wiki/Aqua) apologizing (again).

![Aqua looking apologetic][aqua]

---

[Velkhana](https://monsterhunterworld.wiki.fextralife.com/Velkhana) painting.

![Velkhana profile][velkhana]

---

Painting of the [Angel Oak](https://en.wikipedia.org/wiki/Angel_Oak).

![Angel Oak tree][tree]

---

A wild [Milotic](https://bulbapedia.bulbagarden.net/wiki/Milotic_\(Pok%C3%A9mon\))
swimming.

![Milotic swimming][milotic]

---

Portrait of [Marnie](https://bulbapedia.bulbagarden.net/wiki/Marnie).

![Marnie looking sullen][marnie]

---

Warmup Speedpaint of an iceberg.

![Iceberg speedpaint][iceberg]

---

[Diana Cavendish](https://little-witch-academia.fandom.com/wiki/Diana_Cavendish)
drinking tea.

![Diana Cavnedish drinking tea][diana]

---

[Megumin](https://konosuba.fandom.com/wiki/Megumin) striking a pose.

![Megumin posing][megumin]

---

[eris]: 2020-07-21-eris-fullsize.png
[katarina]: 2020-06-20-gardening-katarina.png
[futaba]: 2020-05-26-futaba-cut-in.png
[handler]: 2020-04-18-handler.png
[megumin2]: 2020-01-27-megumin.png
[aqua]: 2020-01-12-aqua-is-sorry.png
[velkhana]: 2020-01-08-velkhana.png
[tree]: 2019-12-30-tree.png
[milotic]: 2019-12-26-milotic.png
[marnie]: 2019-12-11-marnie.png
[iceberg]: 2019-11-26-iceberg.png
[diana]: 2019-11-24-diana.png
[megumin]: 2019-11-18-megumin.png

[csp]: https://www.clipstudio.net/en
