---
title: "Japan Post #8 - Kyoto"
summary: "Visit to Kyoto."
date: 2018-03-01T12:08:00-07:00
draft: false
---

Did a bunch of sightseeing in Kyoto.

![](./temple.jpg)

![](./tickets.jpg)

![](./mural.jpg)

![](./statue.jpg)

![](./bell.jpg)

![](./rocks.jpg)

![](./fish.jpg)

![](./pond.jpg)

![](./stone.jpg)

![](./bamboo.jpg)

Slightly out of frame: 700 people taking the same picture.

![](./bambooforest.jpg)

Some ladies in Kimono taking selfies.

![](./selfie1.jpg)

If I'm going to post stranger selfies, I guess I'm obligated to post one of myself.

![](./selfie2.jpg)

Had some strange but delicious tea in the middle of a garden tour.

![](./teahouse.jpg)

![](./steps.jpg)

![](./distance.jpg)

![](./hills.jpg)

![](./mountains.jpg)

A bunch of paths were blocked off for use by tour groups.

![](./forbidden.jpg)

![](./thing.jpg)

![](./riverstep.jpg)

![](./river.jpg)

![](./underbridge1.jpg)

![](./underbridge2.jpg)

![](./walkingbridge.jpg)

Once again, one single tree going early.

![](./cherry.jpg)

![](./night.jpg)

![](./people.jpg)


On to [Nijo-jo](../9-nijo).
