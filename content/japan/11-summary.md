---
title: "Japan Post #11 - Summary"
summary: "Summary and cost breakdown of my trip to Japan."
date: 2018-03-01T12:11:00-07:00
draft: false
---

## Tokyo
I spent five days in Tokyo. It's a great city. Very clean, very crowded. Posssibly more vending
machines than people. Probably ¥1,000,000 worth of merchendise left out on tables overnight
in Akihabara with no security. I guess theft isn't a huge problem.

## Takayama
Instead of heading straight to Kyoto, I took the train along the northern coast through
Toyama and spent a day in Takayama. A much smaller city than Tokyo. It had a very small-town
feel despite having almost 100,000 people.

## Kyoto
I spent five days in Kyoto, which is also a very nice city. Very different than Tokyo, a fair
bit harder to get around.

## Miyajima
I went through Hiroshima Peace Park on my way to Miyajima. Sadly the museum was closed for
constructions.

I spent the night and following morning on Miyajima Island. I wish I'd had more time there.
It was pretty late in the trip so I was ready to go home.

## Tokyo again
I stayed at a capsule hotel on my last night in Japan. I learned one very important thing:

# NEVER STAY AT A CAPSULE HOTEL

Just don't do it. They are horrible. Think of every aspect of a hotel. At a capsule hotel,
that aspect is bad. Bad beds, bad "rooms", bad storage, bad food, bad bathing, bad pricing,
bad creepy mandatory uniforms, bad poorly fitting mandatory slippers. This was a very
highly rated one too, so I don't think it was just this one.

Just don't. Seriously, don't even think about it. I'd be happy if I can prevent even one person
from making the same mistake.


## Closing thoughts

I've never planned a trip of this magnitude, or done any kind of real traveling solo. It
was absolutely worth it and a ton of fun. 14 days is maybe a little long. I think 10 days
might have been a better length.

It was not very difficult to plan once I just forced myself to do it. Here are step-by-step
instructions on how to plan a trip to Japan:

1. Go to [https://www.japan-guide.com](https://www.japan-guide.com) and just use an
itinerary to figure out generally where to go and how long to stay there.
2. Buy airfare. ANA is a great airline and they have direct flights from Seattle.
3. Buy a JR rail pass.
   * Note: On my second trip in 2019, I didn't bother with the rail pass, and didn't really
   miss it. Not having the JR pass means you can use any rail line, not just JR, without worrying
   about "wasting" money. Ticket machines generally have an English option. Google Maps works really
   well to plan your route, and will display both English and Japanese station names. I'd no longer
   recommend the JR pass unless you are planning on a lot of train travel and the math works out.
4. Book hotels. I used Expedia, who I kind of hate, but it got the job done.
5. Make sure you have a passport. You don't need any advanced visa or anything.
6. Learn the following phrases. I didn't speak a single sentence of English my entire trip,
   nor did I know much more Japanese than this:
   * Good morning
   * Good afternoon
   * Good night
   * Thank you
   * Excuse me

![](./cost.png)

The trip cost me about $3,500 for a two-week trip. Ways to save money that I didn't do:

* Get a cheaper flight. Fly midweek. I've heard you can fly from Canada cheaper.
* Stay in hostels instead of hotels.
* Don't get the green car on the rail pass. 2019 Note: Maybe don't get a rail pass at all.

I'm glad I went, and I already want to go back again. 2019 Note: I did go back. And I want to go back again.
