---
title: "Japan Post #4 - Food"
summary: "The food of Japan."
date: 2018-03-01T12:04:00-07:00
draft: false
---

I love Japanese food. I tried everything put in front of me, and all of it was good. Even the
enoki mushrooms in my ramen were edible.

Absolutely incredible ramen. Broth so fatty and flavorful that I could feel my arteries clogging as I ate.

![](./ramen1.jpg)

![](./ramen2.jpg)

Teriyaki burger at MOS Burger.

![](./mosburger.jpg)

Some weird chicken and egg thing from McDonald's. I ordered the strangest thing on the menu.

![](./mcd.jpg)

Incredible sushi.

![](./sushi.jpg)

I may have eaten a lot.

![](./plates.jpg)

Lots of good street food.

Kobe beef skewer. Tasty, but underwhelming for the price.

![](./kobe.jpg)

Super Princess Crepe. ¥300 more than the Princess Crepe.

![](./crepe.jpg)

Some Dango.

![](./dango.jpg)

Every hotel I stayed at had beer in the vending machines. A brilliant concept.

![](./beer.jpg)

Japanese pizza place that has successfully emulated crappy American pizza.

![](./pizza.jpg)

![](./pizzachips.jpg)

Pretty good sandwiches at the 7/11.

![](./sammy.jpg)

![](./caloriemate.jpg)

I waited in line for 2 hours to eat at this Gyūkatsu place. Amazing, but not worth the wait.

![](./gyukatsu1.jpg)

I had to grill the meat myself.

![](./gyukatsu2.jpg)

No idea what this was. I bought it from a street vendor in Takayama.

![](./whoknows.jpg)

Hida beef (regional specialty of next-door town of Hida) "sushi". Absolutely incredible. Might be
the best thing I've ever eaten. This alone made the trip to Takayama worth it.

![](./hidasuhi.jpg)

Hida beef bowl. It was less blurry in real life.

![](./hidabeefbowl.jpg)

I don't know what I expected for ¥100.

![](./giantbeer.jpg)

Omurice in Kyoto. I couldn't read the menu so I ordered randomly. Nasty surprise to find it filled
with mushrooms.

![](./omurice.jpg)

Super delicious curry from the place next to my hotel in Kyoto. I ate there... a lot.

![](./curry.jpg)

Some green tea that had the consistency of lawn clippings, and some wierd thing filled with green
tea slime. They were both refreshing and delicious.

![](./tea.jpg)

Okonomiyaki. Looks like garbage, tastes amazing.

![](./oko.jpg)

Local shochu sampler at Miyajima. Amazing.

![](./shochu.jpg)

Bonito sashimi.

![](./bonito.jpg)

Deep fried oyster curry.

![](./oyster.jpg)

Quick noodles at Shin-Kobe station.

![](./noodles.jpg)


Time to hit the [arcade](../5-arcade).
