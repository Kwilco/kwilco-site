---
title: "Japan Post #2 - Akihabara"
summary: "Exploring the anime capital of Japan."
date: 2018-03-01T12:01:00-07:00
draft: false
---

I stayed in [Akihabara](https://en.wikipedia.org/wiki/Akihabara), anime nerd capitol of the world.

![Akiba streets](./streets.jpg)

Three seconds left until the doors open! I'm about 70% sure these people were waiting to get into
an [AKB48](https://en.wikipedia.org/wiki/AKB48) show. Getting tickets is supposedly impossible.

![AKB48](./akb48.jpg)

The line wrapped around the block.

![More line](./akb48line.jpg)

Ehh, let's not...

![Maid cafe](./maid.jpg)

A neat-looking underpass:

![Underpass](./underpass.jpg)

Get your anime here!

![Anime](./kono.jpg)

Apparantly Mario Kart is based on real life.

![Mario kart](./kart.jpg)

![These assholes again?](./kart2.jpg)

Akihabara looks pretty awesome at night.

![Akiba nighttime](./night.jpg)

![More nighttime](./night2.jpg)

Some very imposing buildings hiding treasures...
![Yodobashi](./imposing.jpg)

[Shopping...](../3-shopping)
