---
title: "Japan Post #3 - Shopping"
summary: "Shopping in Japan."
date: 2018-03-01T12:03:00-07:00
draft: false
---

View from the terrifying outdoor stairway on the side of Mandarake, a second-hand manga/anime store.

![](./mandarake.jpg)

Get your Freddie Mercury figurines here!

![](./freddie.jpg)

Yodobashi Camera is essentially 8 department stores stacked on top of each other. It might be the
best store ever made.

Literally thousands of phone cases.

![](./cases.jpg)

Speaking of phones, every single one of these several hundred land-line phones on display was
ringing non-stop. I can only imagine the pain and suffering of working on this floor.

![](./phones.jpg)

![](./watch.jpg)

![](./keyboard.jpg)

![](./headphones.jpg)

Anime sounds better when you listen to it with Anime Earbuds!

![](./animeearbuds.jpg)

This strange speaker was blasting Michael Jackson's "Beat It".

![](./mj.jpg)

Awesome Dark Souls statue.

![](./darksouls.jpg)

![](./warhammer.jpg)

![](./rabite.jpg)

A lot of sweet anime figurines in one case.

![](./figs.jpg)

Hundreds of Gundams...

![](./gundam.jpg)

![](./gundam2.jpg)

![](./gundam3.jpg)

![](./gundam4.jpg)

Some cool suitcases. Not as cool as mine covered in Pokemon stickers though.

![](./suitcases.jpg)

I'm glad to know that my AV receiver brand has cute anime mascots.

![](./denon.jpg)

I saw at least a dozen people watching this movie on the plane.

![](./psychic.jpg)

I think this is a better name than **Furious 7**.

![](./skymission.jpg)

A climate-controlled guitar room?

![](./guitar.jpg)

Creepy dancing Star Wars figurines.

![](./starwars.jpg)

My loot. Wouldn't you like to know?!

![](./loot.jpg)


[Hungry](../4-food)?
