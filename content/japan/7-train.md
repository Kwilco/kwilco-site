---
title: "Japan Post #7 - Train Travel"
summary: "Traveling via rail."
date: 2018-03-01T12:07:00-07:00
draft: false
---

If you want to go anywhere in Japan, you're going to be riding the train.

![](./train.jpg)

![](./city.jpg)

![](./river.jpg)

![](./farm.jpg)

A brief wait in Toyama.

![](./toyama1.jpg)

Extreme crowds in Toyama.

![](./toyama2.jpg)

The station in Takayama only opened when the trains showed up.

![](./early.jpg)

Half of a train.

![](./trainhookup.jpg)

Some very well-behaved schoolkids at a train station.

![](./kiddos.jpg)

Critical toilet instructions for train toilets.

![](./instructions.jpg)

![](./mountain.jpg)

![](./water.jpg)

A fancy decorated train car in Kyoto.

![](./kyotocar.jpg)

Hankyu rail station in Kyoto.

![](./hankyu.jpg)

![](./sky1.jpg)

![](./sky2.jpg)

Just like me, this guy was taking train pictures.

![](./myhero.jpg)


Sightseeing in [Kyoto](../8-kyoto).
