---
title: "Japan Post #1 - Seattle to Tokyo"
summary: "Getting from the US to Japan."
date: 2018-03-01T12:00:00-07:00
draft: false
---

I packed pretty light. About five days of clothes. Toiletries. Laptop. PS Vita.

![Belongings](./suitcase.jpg)

It was stereotypical Seattle weather when I left for the airport. After a two minute walk and a 40
minute train I made it to the airport. After showing my passport and filling out some paperwork I
got my boarding pass and went through security.

![Rain](./rain.jpg)

I flew on a Boeing 787 with All Nippon Airways. I have no complaints with ANA, they are a fine
airline.

I got the Japanese-style meal (who is going to eat western-style on a flight to Japan?). Easily the
best thing I've ever eaten on a plane.

![Plane food](./planefood.jpg)

I'm glad they take TFOA seriously, it's about time somebody did.

![Things falling off aircraft](./tfoa.jpg)

10 hours later I landed at Narita and took a bus to the terminal. There were an incredible amount of people
going through customs, but they processed people pretty quickly. They take your fingerprints, copy
your passport, quiz you on your itinerary, and that was that. I greeted the customs officer in Japanese,
and he called my bluff by responding in Japanese. After getting my "temporary visitor" stamp in my
passport I was finally officially in Japan!

![Made it](./airport.jpg)

The first order of business was getting money. I hit up an ATM and withdrew ¥50,000. The exchange
rate is approximately ¥1 to $0.01. It's a little more favorable than that, but it's easy enough to
thing of one yen as one cent. Ten bucks in Japan is a ¥1000! If you have $10,000 in the bank,
congratulations, you're a millionaire in Japan!

![I'm rich](./money.jpg)

Next order of business was getting internet. Without GPS there was no way I was finding my hotel.
Thankfully, there were some predatory SIM card sellers right there in the airport. It was probably
a rip-off, but dropping ¥7000 on an unlimited data SIM was a no-brainer. Unlimited fast internet on
my phone was available everywhere in the country (well, except one podunk town in the middle of
nowhere). Tethering my laptop to it meant I didn't have to deal with terrible hotel internet.

Before my trip, I filled out a long form online to purchase my Japan Rail Pass Exchange Order. It
was mailed to me and I brought it with me. Upon arrival at the rail office, I filled out some more
paperwork. With that paperwork, my passport (containing the temporary visitor stamp, a requirement
to redeem a Rail Pass), and my exchange order, I received: more paperwork to fill out. After that, I
finally got my pass. I sprung extra for the green car (first class) since I would be on the train so
much. It was definitely worth it.

![How to use a JR pass](./railpass.jpg)

With my Rail Pass and my ticket to Tokyo in hand, I headed for the Narita Express train to Tokyo.
After I got to Tokyo Station, I transferred to the Yamanote Line. I have played multiple video games
featuring Tokyo Station, which came in handy, as I basically already knew the layout.

![Narita station](./nrtstation.jpg)

After arriving in Akihabara, I made my way to the hotel. It was about 4am Pacific time at the point
so I was fading pretty fast. I made it to the hotel, and could not for the life of me figure out how
to turn on the lights. Turns out you have to jam your key into this mysterious hole in the wall to
turn on electricity to your room. Logical. I took a shower then immediately passed out to recover my
HP.

![Stupid key thing](./wtf.jpg)

![Hotel bed](./hotel1.jpg)

![Hotel other side](./hotel2.jpg)

Next up, [Akihabara](../2-akihabara).
