---
title: "Japan Post #5 - Arcade"
summary: "Japanese arcades."
date: 2018-03-01T12:05:00-07:00
draft: false
---

## [Aikatsu Stars!](https://en.wikipedia.org/wiki/Aikatsu_Stars!)

I accidentally got addicated to a rhythm/fashion arcade game for little girls. The
premise is that you scan in your ID card, and 4 clothing cards (top, bottom, shoes,
accessory). Scanning in clothing from the same set not only looks cuter, but gets you more
points. Then you plan a rhythm game by hitting the buttons in time to the dancing anime
girls.


![](./diamonds.jpg)

You play little minigames on the touch screen during the songs for more points. In this
case, you fling the star on the bottom screen up into the star she's holding. If it bounces
around it changes colors. Get the right color and you get more loot. More on loot in a moment.

![](./stars.jpg)

Here's me getting what seems like a lot of points. The anime girls are pretty stoked about
that.

![](./winning.jpg)

At the end of every song, you get some semi-random loot depending on how well you did. You
pick which item you want to keep, customize a card (character, pose, decorations, timestamp).

![](./loot.jpg)

And at the end it prints out your card into the real world! Next time you play, you can scan
in your cards and they will wear them next time. Check out this SPR (super rare) dress.

![](./superrare.jpg)

I recorded a 10-minute video of this sickenly cute game. Think you can make it to the end?

<iframe
 width="560" height="315"
 src="https://www.youtube-nocookie.com/embed/piFBcc0NuLM?rel=0"
 frameborder="0"
 allow="autoplay; encrypted-media"
 allowfullscreen>
</iframe>

Here's a few cards I got.

![](./cards.jpg)

Here's... a few more cards I got. Not pictured: my entire collection of 65 cards. I really
like this game.

![](./smallcollection.jpg)

If you scan cards from the same set (in the case, the *Romance Kiss* set) enough times,
you get the option to pay another ¥100 yen and get a special accessory from that set that
can't be obtained any other way. I don't know what she's saying, but she's pretty excited to
tell me about the *Romance Kiss* accessory I'm about to get.

![](./12times.jpg)

And **I'm** pretty excited about getting some wings. Some other sets: *Gothic Victoria* (my
fav), *Royal Sword*, *My Little Heart*, *Angel Sugar*, and *Shiny Smile*. I got the bonus
accessories for most of them.

![](./freecard.jpg)

The game is of course internet connected. Feel free to browse the profile of my created
character: [♥♪★！ちゃんのマイページ](http://dcd.sc/j1?i=JR3ZTzOZNZ_KmMmEyAAlWlZVTTi6se2qSMBBH9mcS9BSyWm_JKPd6lV57CuTig4X.)

Looking back, me getting addicted to a Japanese rhythm game is probably the least surprising
outcome of this trip. Unfortunately there do not appear to be any *Aikatsu Stars!* machines
in the states. :(

---

This is basically the same game, but with a knock-off power rangers skin, presumably targeted
at boys. Except there's no actual game component and it's just a slot machine.

![](./samegame.jpg)

This is a piano game where notes come flying at you and you press the right keys. On the
lower difficulties you can hit any note close to the right one. On hard you have to hit the
actual note. At that point you are basically just playing the Piano.

![](./piano.jpg)

My first-ever game of [Maimai](https://en.wikipedia.org/wiki/Maimai_\(video_game\)) in which I
full-combo (no misses) this amazing and terrible Sonic Adventure 2 rock song I was obsessed
with as a teenager. I guess I still I remember it.

![](./liveandlearn.jpg)

The song, for reference purposes.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3gPBmDptqlQ?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Not pictured: Sound Voltex, which is awesome.


Probably time to go [outside](../6-ueno).
