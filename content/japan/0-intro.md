---
title: "Japan Post #0 - Start here!"
summary: "A journal of my February 2018 trip to Japan."
date: 2018-05-01T12:00:00-07:00
draft: false
---

![Welcome to Japan](japan.jpg)

This is a writeup of my February 2018 trip to Japan, thanks for stopping by.
I took about 600 pictures. These are some of the better ones.

If you have any corrections to make, please let me know.

Step one: [Getting to Japan](../1-to-tokyo).
