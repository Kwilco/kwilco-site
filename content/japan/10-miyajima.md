---
title: "Japan Post #10 - Miyajima"
summary: "Visit to a small island."
date: 2018-03-01T12:10:00-07:00
draft: false
---

There are two separate ferry systems competing to take you to the island. Either keeping the
price low through competition or working together to fix the price high. Thankfully I didn't
have to find out. My rail pass covered the ferry run by JR.

![](./ferry1.jpg)

![](./ferry2.jpg)

Really beautiful island.

![](./afternoon.jpg)

Too bad it's overrun with giant rats.

![](./stiltrat.jpg)

Ugly, mangy stilt rats.

![](./infested.jpg)

Pretty cool hotel room.

![](./hotel.jpg)

Nice view of the ferry terminal from the room.

![](./hotelview.jpg)

The Itsukushima Shrine "floating gate" lit up at night.

![](./gatenight.jpg)

The gate at high tide.

![](./gate1.jpg)

![](./gate2.jpg)

Gorgeous view from the ferry back to the mainland.

![](./ferryview1.jpg)

![](./ferryview2.jpg)

![](./ferryview3.jpg)


An overall [summary](../11-summary) of the trip.
