---
title: "Japan Post #9 - Nijo-jo"
summary: "Tour of Nijo-jo castle."
date: 2018-03-01T12:09:00-07:00
draft: false
---

Welcome to Nijo castle.

![](./moat1.jpg)

![](./entrance.jpg)

![](./cornertower.jpg)

![](./gate.jpg)

Photos were prohibited in the palace proper. I snuck this one of the landscaping.

![](./forbidden.jpg)

![](./building.jpg)

![](./pond1.jpg)

![](./pond2.jpg)

Some strange covering for plants.

![](./wat.jpg)

![](./map.jpg)

Two moats are better than one.

![](./moat2.jpg)

![](./view.jpg)

![](./moat3.jpg)

Kids with matching hats.

![](./hats.jpg)

A big 'ol fortified gate.

![](./fortifiedgate.jpg)


Onwards to [Miyajima](../10-miyajima).
