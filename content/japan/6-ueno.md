---
title: "Japan Post #6 - Ueno Park"
summary: "Ueno Park and Zoo."
date: 2018-03-01T12:06:00-07:00
draft: false
---

[Ueno Park](https://en.wikipedia.org/wiki/Ueno_Park) is a park in Tokyo that's pretty cool.

![](./map.jpg)

![](./egg.jpg)

A single cherry tree decided it was go time.

![](./cherry.jpg)


## Ueno Park Zoo

There's a pretty cool zoo in the park.

![](./penguins.jpg)

![](./flamingos.jpg)

![](./mole.jpg)

![](./tightrope.jpg)

I have no idea what this monstrosity is.

![](./monster.jpg)


## Bentendo Hall - [Link](http://www.asiarediscovery.com/japan/117-tokyo-ueno-park-bentendo-benten-hall-temple-a-benzaiten)

A pretty cool Bhuddist temple. Didn't get any good pictures of the temple itself, but the
surroundings were interesting.

![](./stone1.jpg)

![](./stone2.jpg)

![](./stone3.jpg)

![](./messageboard.jpg)

![](./papermessages.jpg)


Leaving Tokyo on a [train](../7-train).
