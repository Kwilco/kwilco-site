I finished my first playthrough of Code Vein and wanted to write down my thoughts.


## The Good

* The character creator is pretty awesome. You have a lot of options, and can
modify your outfit after starting the game.
* The blood code system is really fun. It really encourages you to take each
new one for a spin in order to master the new abilities. It's pretty cool once
you have a big pool of abilities to make heavily customized builds with.
* I didn't try the multiplayer, but not having non-consensual pvp like the Souls
games is a huge positive in my book.
* The NPC allies were really fun. They have a lot of relevant chatter out in the
world, and they are extremely effective in combat.
* The mapping system is really nice. Every souls-like game should steal this
system.
* Marking side quest NPCs and items on the map is very nice.
* Cinematic drain attacks look so awesome.
* Not allocating stat points on level up is really nice, it ties in well with
the blood code system letting you completely experiment.


## The Mixed

* Some of the level design is great, some of it is pretty rotten. Just because
every Souls game has a crappy swamp doesn't mean that needs to be copied.
* Combat is kind of a mixed bag. The movesets and mechanics felt kind of overwrought
to me. A lot of the enemies are more annoying than fun.
* High frame rate support is great. But there are some major physics issues.
Most glaring is the fact that elevators just don't work at 144Hz, and require
you to lower the frame rate to get them to move.


## The bad

* Abilities can take way too long to level up. The game also never tells you that
if you level up too much, your abilities start leveling slower (since they gain exp
relative to the enemies of an area).
* A lot of backstory is presented to you in these flashbacks where you walk
around really slowly and look at statues and listen to events from characters'
past. This might not be so bad, but you have to do this 50+ times, and they often
have this really saccharine music playing which makes it hard to take seriously.
* The emotional impact of a bunch of "heroic sacrifice" moments is completely
undermined by pressing the A button on a statue (if you picked up all the hidden
collectables in the area and watched all the memories) and watching the person
just come back to life.
* The cutscenes suffer from really amateurish camera work and pacing. Shots linger
too long, dialogue rambles, a lot of time is spent on posturing. They could have
really used an editor. Most cutscenes could have been cut in half or more and would
have been much more effective.


## Final thoughts

Overall I enjoyed it. It certainly has its flaws, but it was pretty fun to play
through this "easy anime Dark Souls". I'll probably do a New Game+ playthrough.
