# FFXIV Warrior Crystaline Conflict PvP

- this is a very roughly organized list of useful things I learned grinding CC to series level 25 on WAR.
- it does not apply to frontlines. fuck frontlines.
- the intention is to help you win matches, which will save a lot of time over the long run, and also keep things engaging.
- i have no particular PvP credentials to speak of. most of my matches were unranked, but most of it should apply to ranked as well.

this is not a guide on what the buttons do, or how to play at a basic level. other sources cover that quite well:

- https://na.finalfantasyxiv.com/jobguide/warrior/#pvp
- https://www.icy-veins.com/ffxiv/warrior-pvp-guide

## Mindset

- don't worry about your score. you are there to set up opportunities for your team.
- tanks are often the first to die. make peace with that. you being the first to die means your DPS is still alive and dealing damage
- you might do everything right and fail miserably. your team might fail to capitalize on a great setup. the enemy team might play better. good matchmaking means around a 50% win rate
- **always try for the win**. matches 5 minutes on average. it's not okay to afk a losing match. ever. i've seen SO many matches turn around with the crystal an inch from the end

## Preparation

- there's an NPC at wolves den who describes in-detail all the map mechanics. read these thoroughly lest you get wrecked by something you could have read about beforehand
- learn all the job LBs. many of them are really easy to counteract if you pay attention
- if you are on a controller, you probably want to be using legacy type movement to avoid backpedaling
- consider setting the enemy health bars to initials only. way easier to see and distinguish friend from foe
- keep an eye on the team healthbars. consider swapping positions of the kill feed and enemy team bars
- if you spam queue, you will run into a lot of the same players repeatedly. identify weak players and prey on them. if you see someone backpedaling, that's a solid indicator they are not on legacy controls and may be easy pickings. if you feel bad about it, think about it this way: you are helping them get their series exp that much faster
- if you are feeling sweaty, call or mark targets

## Match start

- be very deliberate about when and how you initiate at the start of each match. if you can get a good cluster of people, bloodwhetting + primal rend them, and be prepared to immediately guard if you get focused
- if an ememy gets too greedy trying to get in range, you can grab them with blota. if you time it right, a primal rend stun can often get a kill without them getting a chance to heal at all. it will also often force the enemy team to commit to combat at a disadvantage

## Limit break

- your LB is a cone with more range than you might think. adjust your range to catch more people
- your LB *removes* guard, not just blocks it. this surprises a lot of enemies
- if multiple enemies are guarding and help is on the way, consider popping it to finish them off and turn the numbers in your favor
- spicy tactic on the map with the wind knock-up: wait for people to guard the wind, then LB them. you will also fly up, but catching multiple people off guard who might die outright from fall damage can definitely be worth the trade
- make sure to spam fell cleave when your LB is active. it absolutely slaps, make sure to keep that GCD rolling. this is your time to 1v1 people or pick up that pentakill

## Onslaught

- consider onslaught instead of primal rend on single target if you think you might have a better primal rend opportunity within a few seconds
- onslaught gives 10% extra damage on an enemy. if you really want them dead, consider weaving it in even if you don't need the movement
- onslaught is great for escaping a fight
- if you are winning a team fight, consider trying to stagger kill timing. this will stagger their respawn times. a good team will wait to regroup, which means free progress. a less good team will stream in one-at-a-time which is an almost guaranteed win. one way to do this is let them think they are about to get away, then blota, then let them run, then onslaught/primal rend

## General

- remember to line-of-sight casters, they are usually your most dangerous opponents as WAR
- do not be afraid to run away and elixir. this is usually preferable to dying
- **if your team is down on people, strongly consider letting the enemy push the crystal so you can regain even numbers**. there are exceptions, but **going in solo is the number one loser of games**
- do not sit on the crystal for no reason. only one person needs to be on it to push. bunching up enables the wide variety of AoE to hit your team harder
- don't obsess about pushing the crystal during a 5v5. it's usually not worth compromising your positioning. remember they have to push it through you and cannot push when they are dead
- that said, feel free to block progress by standing on the crystal, or guarding on it when things are dire and reinforcments are imminent
- always know where the crystal is and consider whether you should be pushing or fighting
- always know where your team is and where the enemy is, and what the general momentum is like
- use your respawn time to decide whether to rush back into the fight or wait for more dead teammates to respawn
- **multiple people on the checkpoint makes the checkpoint unlock faster**

## Winning

- when pushing in the crystal for the win, consider pushing towards the enemy spawn. primal rend + blota can outright prevent the enemy from getting on the crystal. this can be really risky, so only do this if you are paying attention to respawn timers
- don't be afraid to LB + blota a lone defender off the crystal to win
- a well-placed primal rend can instantly win a match in overtime if the enemy team is behind
